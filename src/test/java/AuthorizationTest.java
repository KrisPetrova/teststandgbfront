
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AuthorizationTest {
    WebDriver driver;
    AuthorizationPage authorizationPage;

    private final static String loginURL = "https://test-stand.gb.ru/login";
    private final String usernameCorrect = "NininaNina";
    private final String passwordCorrect = "9a013a1f14";

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void initDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(chromeOptions);
        WebDriverManager.chromedriver();
        driver.get(loginURL);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        authorizationPage = new AuthorizationPage(driver);
    }

    @Test
    void correctLogin() {
        authorizationPage.login(usernameCorrect, passwordCorrect);
        new PostsPage(driver).assertPostsPage();
    }

    @Test
    void lengthValidationLogin_2() {
        authorizationPage.login("ss", "dmla");
        authorizationPage.assertLengthValidation();
    }

    @Test
    void lengthValidationLogin_21() {
        authorizationPage.login("sssssssssssssssssssss", "dmla");
        authorizationPage.assertLengthValidation();
    }

    @Test
    void nonExistentValidationLogin() {
        authorizationPage.login("ssssssssssssssssss", "dmla");
        authorizationPage.assertNonExistentValidation();
    }

    @Test
    void symbolValidationLogin() {
        authorizationPage.login("%#", "dmla");
        authorizationPage.assertSymbolValidation();
    }

    @Test
    void emptylValidationLogin() {
        authorizationPage.login("", "");
        authorizationPage.assertEmptylValidation();
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    public AuthorizationTest() {
        super();
    }
}
